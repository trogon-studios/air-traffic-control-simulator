#include "stdafx.h"

const float airplaneMargin = 0.035f;
const float airplaneSize = 0.016f;

void collisionUpdate(Airplane* a1, Airplane* a2) {
	if (bcCollision(a1->currentPosition, airplaneMargin, a2->currentPosition, airplaneMargin) == 1) {
		if (a1->status > -2)
			a1->status = -2;
		if (a2->status > -2)
			a2->status = -2;
	}
	if (bcCollision(a1->currentPosition, airplaneSize, a2->currentPosition, airplaneSize) == 1) {
		if (a1->status > -3)
			a1->status = -3;
		if (a2->status > -3)
			a2->status = -3;
	}
}

void collisionsUpdate(Sky* sky) {
	int i, j, count = sky->airplanesCount;
	for (i = 0; i < count; i++) {
		// If airplane is active
		if (sky->airplanes[i].status != 0) {
			// Reset if not changed
			if (sky->airplanes[i].status > 0)
				sky->airplanes[i].status = -1;
			// Mark collisions
			for (j = i + 1; j < count; j++) {
				// If airplane is active, mark collision if exists
				if (sky->airplanes[j].status != 0)
					collisionUpdate(sky->airplanes + i, sky->airplanes + j);
			}
			// Commit current changes
			if (sky->airplanes[i].status < 0)
				sky->airplanes[i].status = abs(sky->airplanes[i].status);
		}
	}
}

void approachingUpdate(Sky* sky) {
	Vector2 vA = { 0.0f, 0.01f };
	vA = vector2Rotated(vA, runwayAngle);

	Vector2 vB = { 0.675f, 0.0f };
	vB = vector2Rotated(vB, runwayAngle);

	int i, count = sky->airplanesCount;
	for (i = 0; i < count; i++) {
		double lenFromA = lengthPointToVector(vA, vector2Subtraction(Vector2_Zero, vA), sky->airplanes[i].currentPosition);
		double lenFromB = lengthPointToVector(vB, vector2Subtraction(Vector2_Zero, vA), sky->airplanes[i].currentPosition);
		double lenFromC = lengthPointToVector(vA, vector2Addition(vector2Subtraction(Vector2_Zero, vA), vB), sky->airplanes[i].currentPosition);
		double lenFromD = lengthPointToVector(vB, vector2Addition(Vector2_Zero, vA), sky->airplanes[i].currentPosition);

		if ((lenFromC + lenFromA <= 0.676f) &&
			(lenFromB + lenFromD <= 0.031f)) {
			sky->airplanes[i].isApproaching = true;
		} else {
			sky->airplanes[i].isApproaching = false;
		}
	}
}

Sky* skyInit( ) {
	Sky* sky = (struct _sky*)malloc(sizeof(struct _sky));
	return sky;
}

void skyUpdate(GameTime gameTime, Sky* sky) {
	static int k = 0;
	static double time = 0;
	time += gameTime.elapsedGameTime;
	k++;

	int i, count = sky->airplanesCount;

	if (time > 0.7) {
		GameTime gt = { gameTime.isRunningSlowly, time / k, gameTime.totalGameTime };
		for (i = 0; i < count; i++) {
			if (sky->airplanes[i].status != 0)
				airplaneUpdate(gt, sky->airplanes + i);
		}
		collisionsUpdate(sky);
		approachingUpdate(sky);
		time = 0;
		k = 0;
	}
}

void skyDraw(Sky sky) {
	int i, count = sky.airplanesCount;

	for (i = 0; i < count; i++) {
		if (sky.airplanes[i].status != 0)
			airplaneDraw(sky.airplanes[i]);
	}
}

void skyControl(Sky* sky, char* identity, char* command, char* instruction) {
	int i, count = sky->airplanesCount;

	if (strlen(command) > 0) {
		for (i = 0; i < count; i++) {
			if (strcmp(sky->airplanes[i].name, identity) == 0) {
				int d = 0;
				sscanf(instruction, "%d", &d);
				switch (command[0]) {
					case 'a':
						airplaneSetAltitude(sky->airplanes + i, d);
						break;
					case 'd':
						airplaneSetDirection(sky->airplanes + i, d);
						break;
					case 's':
						airplaneSetSpeed(sky->airplanes + i, d);
						break;
				}
			}
		}
	}
}
