#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <GL/freeglut.h>

#include "color.h"
#include "constrains.h"
#include "string.h"
#include "vector2.h"
#include "framework.h"
#include "screen.h"

#include "gameTime.h"

#include "airplane.h"
#include "sky.h"

#include "game.h"
#include "menu.h"
