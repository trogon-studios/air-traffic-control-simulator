#include "stdafx.h"

const int contextMenuEntryCount = 2;
const int mainMenuEntryCount = 3;

void menuFinalDrawPosition(Game game, int id) {
	Color color = White;
	Vector2 textPosition = { 5.0f, -25.0f * id };
	switch (id) {
		case 0:
			if(game.points > 0){
				drawText(textPosition, "You win!", color);
			}else{
				drawText(textPosition, "Game over", color);
			}
			break;
		case 1:
			drawText(textPosition, stringConcat("Total points: ", intToString(game.points), false), White);
			break;
	}
}

void menuInstructionDrawPosition(Game game, int id) {
	Color color = White;
	Vector2 textPosition = { -game.screen->width * 0.35f, -25.0f * id };
	switch (id) {
		case 0:
			drawText(textPosition, "Instruction:", color);
			break;
		case 1:
			drawText(textPosition, "Enter - confirm", color);
			break;	
		case 2:
			drawText(textPosition, "Escape - back", color);
			break;
		case 4:
			drawText(textPosition, "In game commands:", color);
			break;	
		case 5:
			drawText(textPosition, "<label> speed <value> - set speed (200, 600)", color);
			break;
		case 6:
			drawText(textPosition, "<label> direction <value> - set direction (0, 360)", color);
			break;
	}
}

void menuContextDrawPosition(Game game, int id) {
	Color color = White;
	Vector2 textPosition = { 5.0f, -25.0f * id };
	if (id == game.menuSelected) {
		color = Yellow;
	}
	switch (id) {
		case 0:
			drawText(textPosition, "Return", color);
			break;
		case 1:
			drawText(textPosition, "Back to menu", color);
			break;
	}
}

void menuDrawPosition(Game game, int id) {
	Color color = White;
	Vector2 textPosition = { 5.0f, -25.0f * id };
	if (id == game.menuSelected) {
		color = Yellow;
	}
	switch (id) {
		case 0:
			drawText(textPosition, "New game", color);
			break;
		case 1:
			drawText(textPosition, "Instruction", color);
			break;
		case 2:
			drawText(textPosition, "Exit", color);
			break;
	}
}

void menuContextUpdate(Game* game) {
	static bool lastKeyUp = false;
	static bool lastKeyDown = false;

	if (lastKeyDown != true && game->keyStatus[keyDown] == true && game->menuSelected < (contextMenuEntryCount - 1)) {
		game->menuSelected++;
	}
	if (lastKeyUp != true && game->keyStatus[keyUp] == true && game->menuSelected > 0) {
		game->menuSelected--;
	}

	lastKeyUp = game->keyStatus[keyUp];
	lastKeyDown = game->keyStatus[keyDown];
}

void menuUpdate(Game* game) {
	static bool lastKeyUp = false;
	static bool lastKeyDown = false;

	if (lastKeyDown != true && game->keyStatus[keyDown] == true && game->menuSelected < (mainMenuEntryCount - 1)) {
		game->menuSelected++;
	}
	if (lastKeyUp != true && game->keyStatus[keyUp] == true && game->menuSelected > 0) {
		game->menuSelected--;
	}

	lastKeyUp = game->keyStatus[keyUp];
	lastKeyDown = game->keyStatus[keyDown];
}

void menuFinalDraw(Game game) {
	screenOrthoClipping(*game.screen);
	int i;
	for (i = 0; i < 2; i++) {
		menuFinalDrawPosition(game, i);
	}
}

void menuInstructionDraw(Game game) {
	screenOrthoClipping(*game.screen);
	int i;
	for (i = 0; i < 7; i++) {
		menuInstructionDrawPosition(game, i);
	}
}

void menuContextDraw(Game game) {
	screenOrthoClipping(*game.screen);
	int i;
	for (i = 0; i < contextMenuEntryCount; i++) {
		menuContextDrawPosition(game, i);
	}
}

void menuDraw(Game game) {
	screenOrthoClipping(*game.screen);
	int i;
	for (i = 0; i < mainMenuEntryCount; i++) {
		menuDrawPosition(game, i);
	}
}
