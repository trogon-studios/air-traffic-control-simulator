#include "stdafx.h"

const float airplaneScale = 0.015f;
const int minAltitude = 100;
const int maxAltitude = 13000;
const int minSpeed = 200;
const int maxSpeed = 600;
const double maxAngle = PI / 16;
const float maxSpeedShift = 10;

Airplane* airplaneInit( ) {
	Airplane* airplane = (struct _airplane*)malloc(sizeof(struct _airplane));
	airplane->name = (char*) calloc(8, sizeof(char));

	float rand_max = (float) RAND_MAX;
	float altitute = rand( ) / rand_max * (maxAltitude - minAltitude) + minAltitude;
	int direction = (int) (rand( ) / rand_max * 360);
	float speed = rand( ) / rand_max * (maxSpeed - minSpeed) + minSpeed;

	airplane->currentAltitude = altitute;
	airplane->currentDirection = airplaneDirectionTranslate(direction);
	airplane->currentSpeed = speed;

	airplane->currentPosition = Vector2_Zero;

	airplane->status = 1;

	airplane->isApproaching = 0;
	airplane->approachDirection = airplaneDirectionTranslate(0);

	airplane->nextDirection = airplane->currentDirection;
	airplane->nextAltitude = (int) airplane->currentAltitude;
	airplane->nextSpeed = (int) airplane->currentSpeed;

	return airplane;
}

void airplaneUpdate(GameTime gameTime, Airplane* airplane) {
	if (airplane->isApproaching == true) {
		airplane->currentDirection = airplaneDirectionTranslate(270 - runwayAngle);
		airplane->nextSpeed = 100;
	} else {
		double directAngle = vector2Angle(airplane->currentDirection, airplane->nextDirection);
		if (directAngle != 0) {
			double minAngle = maxAngle - absd(directAngle);
			if (minAngle > 0) {
				if (directAngle > 0) {
					airplane->currentDirection = vector2Rotate(airplane->currentDirection, minAngle);
				} else {
					airplane->currentDirection = vector2Rotate(airplane->currentDirection, -minAngle);
				}
			} else {
				if (directAngle > 0) {
					airplane->currentDirection = vector2Rotate(airplane->currentDirection, maxAngle);
				} else {
					airplane->currentDirection = vector2Rotate(airplane->currentDirection, -maxAngle);
				}
			}
		}
	}
	float speedShift = airplane->nextSpeed - airplane->currentSpeed;
	if (absd(speedShift) > maxSpeedShift) {
		if (speedShift > 0) {
			airplane->currentSpeed += maxSpeedShift;
		} else {
			airplane->currentSpeed -= maxSpeedShift;
		}
	}
	float speed = airplane->currentSpeed / 2500.f * (float) gameTime.elapsedGameTime;
	Vector2 newPosition = { airplane->currentPosition.x + speed * airplane->currentDirection.x, airplane->currentPosition.y + speed * airplane->currentDirection.y };
	airplane->currentPosition = newPosition;
}

void airplaneDraw(Airplane airplane) {
	if (airplane.status > 0) {
		switch (airplane.status) {
			default:
				if (airplane.isApproaching == true) {
					drawAirplane(airplane.currentPosition, airplaneScale, LightBlue);
				} else {
					drawAirplane(airplane.currentPosition, airplaneScale, Blue);
				}
				break;
			case 2:
				drawAirplane(airplane.currentPosition, airplaneScale, Red);
				break;
			case 3:
				drawAirplane(airplane.currentPosition, airplaneScale, Green);
				break;
		}
		drawText(airplane.currentPosition, airplane.name, White);
	}

}

Vector2 airplaneDirectionTranslate(int directionDegree) {
	Vector2 direction = Vector2_UnitY;
	return vector2Rotated(direction, -directionDegree);
}

void airplaneSetAltitude(Airplane* airplane, int newAltitude) {
	if (newAltitude >= minAltitude && newAltitude <= maxAltitude) {
		airplane->nextAltitude = newAltitude;
	} else if (newAltitude < minAltitude) {
		airplane->nextAltitude = minAltitude;
	} else if (newAltitude > maxAltitude) {
		airplane->nextAltitude = maxAltitude;
	}
}

void airplaneSetDirection(Airplane* airplane, int newDirection) {
	airplane->nextDirection = airplaneDirectionTranslate(newDirection);
}

void airplaneSetSpeed(Airplane* airplane, int newSpeed) {
	if (newSpeed >= minSpeed && newSpeed <= maxSpeed) {
		airplane->nextSpeed = newSpeed;
	} else if (newSpeed < minSpeed) {
		airplane->nextSpeed = minSpeed;
	} else if (newSpeed > maxSpeed) {
		airplane->nextSpeed = maxSpeed;
	}
}
