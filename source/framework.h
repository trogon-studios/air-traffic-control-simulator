// Framework

#ifndef __framework
#define __framework

// Zwraca warto�� bezwzgl�dn� z liczby typu double
double absd(double value);

int intMax(int i1, int i2);

// Sprawdzenie kolizji (boundingCircle) (0 - brak, 1 - kolizja)
bool bcCollision(Vector2 p1, float r1, Vector2 p2, float r2);

// Sprawdzenie kolizji (boundingBox) (0 - brak, 1 - kolizja)
bool bbCollision(Vector2 v1, Vector2 v2);

// Narysowanie linii na ekranie
void drawLine(Vector2 positionBegin, Vector2 positionEnd, Color foreground);

// Narysowanie okr�gu na ekranie
void drawCircle(Vector2 position, float radius, int sides, Color foreground);

// Narysowanie punktu na ekranie
void drawPoint(Vector2 position, float radius, Color foreground);

// Narysowanie tekstu na ekranie
void drawText(Vector2 position, const char* text, Color foregorund);

void drawAirplane(Vector2 position, float scale, Color foregorund);

void drawRadar(Vector2 position, Vector2 lineCursor, int runwayAngle, float scale);

void drawAirport(Vector2 position, float scale);

void drawRunwayPath(Vector2 position, int angle, float scale);

double lengthPointToVector(Vector2 v1, Vector2 p1, Vector2 p2);

#endif
