﻿#include "stdafx.h"

double absd(double value) {
	if (value > 0)
		return value;
	return -1.0 * value;
}

int intMax(int i1, int i2) {
	if (i1 > i2)
		return i1;
	else
		return i2;
}

bool bcCollision(Vector2 p1, float r1, Vector2 p2, float r2) {
	if (vector2Length(vector2Subtraction(p1, p2)) <= r1 + r2) {
		return true;
	}
	return false;
}

bool bbCollision(Vector2 v1, Vector2 v2) {
	float halfSize = 0.03f;
	float k1_Min_X = v1.x - halfSize;
	float k1_Max_X = v1.x + halfSize;
	float k1_Min_Y = v1.y - halfSize;
	float k1_Max_Y = v1.y + halfSize;
	float k2_Min_X = v2.x - halfSize;
	float k2_Max_X = v2.x + halfSize;
	float k2_Min_Y = v2.y - halfSize;
	float k2_Max_Y = v2.y + halfSize;

	if ((k1_Max_X >= k2_Min_X) &&
		(k1_Min_X <= k2_Max_X) &&
		(k1_Max_Y >= k2_Min_Y) &&
		(k1_Min_Y <= k2_Max_Y)) {
		return true;
	}
	return false;
}

void drawLine(Vector2 position1, Vector2 position2, Color foreground) {
	glColor3f(foreground.red, foreground.green, foreground.blue);
	glBegin(GL_LINES);
	glVertex3f(position1.x, position1.y, 0.0);
	glVertex3f(position2.x, position2.y, 0.0);
	glEnd( );
}

void drawCircle(Vector2 center, float radius, int sides, Color foreground) {
	glColor3f(foreground.red, foreground.green, foreground.blue);
	glBegin(GL_LINE_LOOP);
	Vector2 radius2 = { radius, 0 };
	double angle = 2.0 * PI / sides;
	int a;
	for (a = 0; a < sides; a++) {
		glVertex2d(radius2.x + center.x, radius2.y + center.y);
		radius2 = vector2Rotate(radius2, angle);
	}
	glEnd( );
}

void drawPoint(Vector2 position, float radius, Color foreground) {
	glColor3f(foreground.red, foreground.green, foreground.blue);
	glPointSize(radius);
	glBegin(GL_POINTS);
	glVertex2d(position.x, position.y);
	glEnd( );
}

void drawText(Vector2 position, const char* text, Color foreground) {
	glColor3f(foreground.red, foreground.green, foreground.blue);
	glRasterPos2d(position.x, position.y);
	glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (const unsigned char*) text);
}

void drawAirplane(Vector2 position, float scale, Color foreground) {
	drawCircle(position, scale, 50, foreground);
	glBegin(GL_POLYGON);
	glVertex2d(-0.15f * scale + position.x, +0.25f * scale + position.y);
	glVertex2d(-0.15f * scale + position.x, +0.75f * scale + position.y);
	glVertex2d(+0.15f * scale + position.x, +0.75f * scale + position.y);
	glVertex2d(+0.15f * scale + position.x, +0.25f * scale + position.y);
	glVertex2d(+0.65f * scale + position.x, -0.55f * scale + position.y);
	glVertex2d(-0.65f * scale + position.x, -0.55f * scale + position.y);
	glEnd( );
}

void drawRadar(Vector2 position, Vector2 lineCursor, int runwayAngle, float scale) {
	glLineWidth(2);

	int i, count = 6;
	float p = scale / count;
	for (i = 1; i < count; i++) {
		drawCircle(position, p * i, 100, DarkGreen);
	}

	drawCircle(position, scale, 100, Yellow);
	drawLine(position, lineCursor, Yellow);
	glColor3f(LightBlue.red, LightBlue.green, LightBlue.blue);
	drawRunwayPath(position, runwayAngle, scale * 0.75f);
}

void drawAirport(Vector2 position, float scale) {
	glBegin(GL_POINTS);
	glVertex2d(position.x, position.y);
	glEnd( );
}

void drawRunwayPath(Vector2 position, int angle, float scale) {
	Vector2 lineA = { 0.0f, -0.01f };
	lineA = vector2Rotated(lineA, angle);

	Vector2 lineB = { 0.8f, 0.0f };
	lineB.x = scale;
	lineB = vector2Rotated(lineB, angle);

	glLineStipple(5, 0xAAAA);
	glEnable(GL_LINE_STIPPLE);
	glBegin(GL_LINES);

	glVertex2d(position.x - lineA.x, position.y - lineA.y);
	glVertex2d(position.x - lineA.x + lineB.x, position.y - lineA.y + lineB.y);
	glVertex2d(position.x + lineA.x, position.y + lineA.y);
	glVertex2d(position.x + lineA.x + lineB.x, position.y + lineA.y + lineB.y);

	glEnd( );
	glDisable(GL_LINE_STIPPLE);
}

double lengthPointToVector(Vector2 v1, Vector2 p1, Vector2 p2) {
	// 1. Wyznaczenie prostej z v1 i punktu początkowego wektora p1
	double a1 = (v1.y / v1.x);
	double b1 = p1.y - p1.x * a1;

	// 2. Wyznaczenie prostej prostopadłej przechodzącej przez p2
	double a2 = -(v1.x / v1.y);
	double b2 = p2.y - p2.x * a2;

	// 3. Rozwiązanie równania
	double x = (b2 - b1) / (a1 - a2);
	double y = a1 * x + b1;
	Vector2 p3;
	p3.x = (float) x;
	p3.y = (float) y;

	// 4. Wyznaczenie wektora p2 -> p3
	Vector2 v2 = vector2Subtraction(p3, p2);

	// 5. Obliczenie długości wektora v2
	double result = vector2Length(v2);
	return result;
}
