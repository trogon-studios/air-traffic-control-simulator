// String

#ifndef __string
#define __string

// Inicjacja pami�ci celem przechowywania tekstu
char* stringInit( );

// Konwersja liczby int na tekst
char* intToString(int value);

// Konwersja liczby double na tekst
char* doubleToString(double value);

// Dodanie znaku na ko�cu tekstu
char* stringAppendChar(char* baseValue, char value, bool destroyBase);

// Usuni�cie znaku z ko�cu tekstu
char* stringRemoveChar(char* baseValue, bool destroyBase);

// Zwraca wycinek tekstu
char* stringSub(char* baseValue, size_t startIndex, size_t endIndex, bool destroyBase);

// ��czy dwa teksty w jeden
char* stringConcat(char* baseValue, char* value, bool destroyBase);

#endif
