#include "stdafx.h"

const float radarScale = 0.9f;
const Vector2 textPoints = { -1.0f, 0.9f };

int runwayAngle = -290 + 90;

Airplane* GenerateAirplanes(int count) {
	int i;

	Airplane *airplanes = (struct _airplane*)malloc(sizeof(struct _airplane) * count);

	for (i = 0; i < count; i++) {
		airplanes[i] = *airplaneInit( );

		airplanes[i].currentPosition = vector2Negation(vector2Multiplication(airplanes[i].currentDirection, radarScale * 0.97f));

		strcpy(airplanes[i].name, intToString(i));
	}

	return airplanes;
}

void DrawFps(GameTime gameTime) {
	drawText(Vector2_Zero, (const char*) doubleToString(1 / gameTime.elapsedGameTime), White);
}

Game* gameInit( ) {
	Game* game = (struct _game*)malloc(sizeof(struct _game));
	game->gameStatus = 1;
	game->menuSelected = 0;
	game->points = 0;
	game->gameTime = gameTimeInit( );
	game->screen = screenInit(0, 0);
	game->sky = skyInit( );
	game->sky->airplanesCount = 15;
	game->sky->airplanes = GenerateAirplanes(game->sky->airplanesCount);
	return game;
}

void gameUpdate(Game* game) {
	static bool lastKeyEnter = false;
	static bool lastKeyEscape = false;
	static double time = 0;
	gameTimeUpdate(game->gameTime);

	switch (game->gameStatus) {
		case 1:
			menuUpdate(game);
			if (lastKeyEnter != true && game->keyStatus[keyEnter] == true) {
				switch (game->menuSelected) {
					case 0:
						game->points = 0;
						free(game->sky->airplanes);
						game->sky->airplanes = GenerateAirplanes(game->sky->airplanesCount);
						game->gameStatus = 2;
						break;
					case 1:
						game->gameStatus = 4;
						break;
					case 2:
						game->gameStatus = 0;
						break;
				}
				game->menuSelected = 0;
			}
			break;
		case 2:
			if (lastKeyEscape != true && game->keyStatus[keyEscape] == true) {
				game->gameStatus = 3;
			} else {
				skyUpdate(*game->gameTime, game->sky);

				time += game->gameTime->elapsedGameTime;
				if (time > 0.99) {
					game->points++;
					time = 0;
				}

				int i, count = game->sky->airplanesCount;
				int activeAirplanes = 0;

				for (i = 0; i < count; i++) {
					Airplane* airplane = game->sky->airplanes + i;
					if (airplane->status > 0) {
						if (airplane->status == 3) {
							game->points -= 250;
							airplane->status = 0;
						} else if (vector2Length(airplane->currentPosition) > radarScale) {
							game->points -= 100;
							airplane->status = 0;
						} else if (airplane->isApproaching == true && airplane->currentSpeed < 150 && vector2Length(airplane->currentPosition) < 0.015f) {
							game->points += 750;
							airplane->status = 0;
						}
						activeAirplanes++;
					}
				}
				if(activeAirplanes == 0){
					gameSavePoints(*game);
					game->gameStatus = 5;
				}
			}
			break;
		case 3:
			menuContextUpdate(game);
			if (lastKeyEscape != true && game->keyStatus[keyEscape] == true) {
				game->gameStatus = 1;
			} else if (lastKeyEnter != true && game->keyStatus[keyEnter] == true) {
				switch (game->menuSelected) {
					case 0:
						game->gameStatus = 2;
						break;
					case 1:
						game->gameStatus = 1;
						break;
				}
				game->menuSelected = 0;
			}
			break;
		case 4:
			if (lastKeyEscape != true && game->keyStatus[keyEscape] == true) {
				game->gameStatus = 1;
			}
			break;
		case 5:
			if (lastKeyEscape != true && game->keyStatus[keyEscape] == true) {
				game->gameStatus = 1;
			}
			break;
	}

	lastKeyEnter = game->keyStatus[keyEnter];
	lastKeyEscape = game->keyStatus[keyEscape];
}

void gameDraw(Game game) {
	static Vector2 line = { 0.9f, 0.0f };

	switch (game.gameStatus) {
		case 1:
			menuDraw(game);
			break;
		case 2:
			line = vector2Rotate(line, -0.015);
			screenOrthoAspectNormalClipping(*game.screen);

			drawRadar(Vector2_Zero, line, runwayAngle, radarScale);
			skyDraw(*game.sky);

			drawText(textPoints, stringConcat("Points: ", intToString(game.points), false), White);
			break;
		case 3:
			menuContextDraw(game);
			break;
		case 4:
			menuInstructionDraw(game);
			break;
		case 5:
			menuFinalDraw(game);
			break;
	}
}

void gameControl(Game* game, char* identity, char* command, char* instruction) {
	skyControl(game->sky, identity, command, instruction);
}

void gameSavePoints(Game game){
	FILE *fp;

	/* open the file */
	fp = fopen("wyniki.log", "a+");
	if (fp != NULL) {		
		time_t rawtime;
		struct tm *timeinfo;

		time(&rawtime);
		timeinfo = localtime(&rawtime);

		/* write to the file */
		fprintf(fp, "%s: %d points\n", asctime(timeinfo), game.points);

		/* close the file */
		fclose(fp);
	}
}
