#include "stdafx.h"

const Color Black = { 0, 0, 0, 1 };
const Color DarkGreen = { 0, 0.45f, 0, 1 };
const Color Red = { 1, 0, 0, 1 };
const Color Green = { 0, 1, 0, 1 };
const Color Blue = { 0, 0, 1, 1 };
const Color Yellow = { 1, 1, 0, 1 };
const Color Yellow2 = { 0, 1, 1, 1 };
const Color Yellow3 = { 1, 0, 1, 1 };
const Color LightBlue = { 0.05f, 0.5f, 1, 1 };
const Color White = { 1, 1, 1, 1 };
