

#ifndef __gameTime
#define __gameTime

typedef struct _gameTime {
	bool isRunningSlowly;
	long double elapsedGameTime;
	long double totalGameTime;
} GameTime;

GameTime* gameTimeInit( );

void gameTimeUpdate(GameTime* gameTime);

#endif
