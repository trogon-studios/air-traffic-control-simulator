


#ifndef __menu
#define __menu

void menuContextUpdate(Game* game);

void menuUpdate(Game* game);

void menuFinalDraw(Game game);

void menuInstructionDraw(Game game);

void menuContextDraw(Game game);

void menuDraw(Game game);

#endif
