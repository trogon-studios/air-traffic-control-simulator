

#ifndef __screen
#define __screen

typedef struct _screen {
	bool isFullscreen;
	int width;
	int height;
	float aspectRatio;
} Screen;

Screen* screenInit();

void screenApply(Screen* screen, int width, int height);

void screenOrtho(Screen screen);

void screenOrthoAspect(Screen screen);

void screenOrthoNormal(Screen screen);

void screenOrthoAspectNormal(Screen screen);

void screenOrthoClipping(Screen screen);

void screenOrthoAspectClipping(Screen screen);

void screenOrthoNormalClipping(Screen screen);

void screenOrthoAspectNormalClipping(Screen screen);

#endif
