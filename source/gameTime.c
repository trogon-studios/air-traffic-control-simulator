#include "stdafx.h"

GameTime* gameTimeInit( ) {
	GameTime* gameTime = (struct _gameTime*)malloc(sizeof(struct _gameTime));
	gameTime->isRunningSlowly = false;
	gameTime->elapsedGameTime = 0;
	gameTime->totalGameTime = 0;
	return gameTime;
}

void gameTimeUpdate(GameTime* gameTime) {
	static long double lastUpdateTime = 0;
	long double totalTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
	gameTime->elapsedGameTime = absd(totalTime - lastUpdateTime);
	gameTime->totalGameTime = totalTime;
	lastUpdateTime = totalTime;
}
