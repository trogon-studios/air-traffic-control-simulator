#include "airplane.h"

#ifndef __sky
#define __sky

typedef struct _sky {
	Airplane* airplanes;
	int airplanesCount;
} Sky;

Sky* skyInit( );

void skyUpdate(GameTime gameTime, Sky* sky);

void skyDraw(Sky sky);

void skyControl(Sky* sky, char* identity, char* command, char* instruction);

#endif
