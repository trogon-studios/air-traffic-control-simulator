
#ifndef __color
#define __color

typedef struct _color {
	float red;
	float green;
	float blue;
	float alpha;
} Color;

const Color Black;
const Color DarkGreen;
const Color Red;
const Color Green;
const Color Blue;
const Color Yellow;
const Color Yellow2;
const Color Yellow3;
const Color LightBlue;
const Color White;

#endif
