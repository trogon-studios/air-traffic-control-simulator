// Constrains

#ifndef __constrains
#define __constrains

#define bool short

#define true 1
#define false 0

#define sqrt2	1.4142135623730950488016887242097
#define halfPI	1.5707963267948966192313216916398
#define sqrt3	1.7320508075688772935274463415059
#define PI		3.1415926535897932384626433832795
#define twoPI	6.2831853071795864769252867665590

// Backspace function key.
const int keyBackspace;

// Enter function key.
const int keyEnter;

// Escape function key.
const int keyEscape;

// F1 function key.
const int keyF1;

// F2 function key.
const int keyF2;

// F3 function key.
const int keyF3;

// F4 function key.
const int keyF4;

// F5 function key.
const int keyF5;

// F6 function key.
const int keyF6;

// F7 function key.
const int keyF7;

// F8 function key.
const int keyF8;

// F9 function key.
const int keyF9;

// F10 function key.
const int keyF10;

// F11 function key.
const int keyF11;

// F12 function key.
const int keyF12;

// Left directional key.
const int keyLeft;

// Up directional key.
const int keyUp;

// Right directional key.
const int keyRight;

// Down directional key.
const int keyDown;

// Page up directional key.
const int keyPageUp;

// Page down directional key.
const int keyPageDown;

// Home directional key.
const int keyHome;

// End directional key.
const int keyEnd;

// Inset directional key.
const int keyInsert;


#endif
