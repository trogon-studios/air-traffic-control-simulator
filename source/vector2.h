

#ifndef __vector2
#define __vector2

typedef struct _vector2 {
	float x;
	float y;
} Vector2;

const Vector2 Vector2_Zero;
const Vector2 Vector2_UnitX;
const Vector2 Vector2_UnitY;
const Vector2 Vector2_One;

double vector2Length(Vector2 v1);

double vector2Arg(Vector2 v1);

double vector2Dot(Vector2 v1, Vector2 v2);

double vector2Angle(Vector2 v1, Vector2 v2);

Vector2 vector2Negation(Vector2 p);

Vector2 vector2Rotate(Vector2 p, double angle);

Vector2 vector2Rotated(Vector2 p, double angle);

Vector2 vector2Addition(Vector2 v1, Vector2 v2);

Vector2 vector2Subtraction(Vector2 v1, Vector2 v2);

Vector2 vector2Multiplication(Vector2 v1, float times);

#endif
