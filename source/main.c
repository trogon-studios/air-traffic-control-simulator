#include "stdafx.h"

Game* game;

char* consoleBuffer;
char* identify;
char* command;
char* instruction;

static void init( ) {
	game = gameInit( );
	consoleBuffer = stringInit( );
	identify = stringInit( );
	command = stringInit( );
	instruction = stringInit( );
}

static void resize(int width, int height) {
	screenApply(game->screen, width, height);
}

static void update(int data) {
	if (game->gameStatus > 0) {
		glutTimerFunc(15, update, data++);

		gameUpdate(game);

		glutPostRedisplay( );
	} else {
		glutExit( );
	}
}

static void draw(void) {
	glClear(GL_COLOR_BUFFER_BIT);

	gameDraw(*game);

	screenOrthoNormalClipping(*game->screen);
	Vector2 pos = { -0.98f, -0.98f };
	drawText(pos, (const char*) consoleBuffer, White);

	glutSwapBuffers( );
}

void keyPressed(unsigned char key, int x, int y) {
	game->keyStatus[key] = true;
	if (game->gameStatus == 2) {
		if (key == keyEnter) {
			sscanf(consoleBuffer, "%s %s %s", identify, command, instruction);

			char* oldConsoleBuffer = consoleBuffer;
			consoleBuffer = stringInit( );
			free(oldConsoleBuffer);
			gameControl(game, identify, command, instruction);
		} else if (key == keyBackspace) {
			consoleBuffer = stringRemoveChar(consoleBuffer, true);
		} else {
			consoleBuffer = stringAppendChar(consoleBuffer, key, true);
		}
	}
}

void keyReleased(unsigned char key, int x, int y) {
	game->keyStatus[key] = false;
}

void skeyPressed(int key, int x, int y) {
	game->keyStatus[key] = true;
}

void skeyReleased(int key, int x, int y) {
	game->keyStatus[key] = false;
}

int main(int argc, char* argv[]) {
	init( );

	screenApply(game->screen, 800, 600);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

	glutCreateWindow("Air Traffic Control Simulator");

	glutReshapeFunc(resize);
	glutDisplayFunc(draw);
	glutKeyboardFunc(keyPressed);
	glutKeyboardUpFunc(keyReleased);
	glutSpecialFunc(skeyPressed);
	glutSpecialUpFunc(skeyReleased);
	glutTimerFunc(15, update, 0);

	glutMainLoop( );

	return 0;
}
