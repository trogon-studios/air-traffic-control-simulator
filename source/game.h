

#ifndef __game
#define __game

typedef struct _game {
	bool keyStatus[256];
	// Status gry: 0- exit, 1- menu, 2- game, 3- context menu, 4- instruction, 5- final screen
	bool gameStatus;
	bool menuSelected;
	int points;
	GameTime* gameTime;
	Screen* screen;
	Sky* sky;
} Game;

int runwayAngle;

Game* gameInit( );

void gameUpdate(Game* game);

void gameDraw(Game game);

void gameControl(Game* game, char* identity, char* command, char* instruction);

void gameSavePoints(Game game);

#endif
