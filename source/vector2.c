#include "stdafx.h"

const Vector2 Vector2_Zero = { 0, 0 };
const Vector2 Vector2_UnitX = { 1, 0 };
const Vector2 Vector2_UnitY = { 0, 1 };
const Vector2 Vector2_One = { 1, 1 };

double vector2LengthSqrt(Vector2 v1) {
	return v1.x * v1.x + v1.y * v1.y;
}

double vector2Length(Vector2 v1) {
	return sqrt(vector2LengthSqrt(v1));
}

// Returns value ( -Pi, Pi ]
double vector2Arg(Vector2 v1) {
	double result = atan2(v1.y, v1.x);
	return result;
}

double vector2Dot(Vector2 v1, Vector2 v2) {
	return v1.x * v2.x + v1.y * v2.y;
}

// Returns value ( -Pi, Pi ]
double vector2Angle(Vector2 v1, Vector2 v2) {
	double result = vector2Arg(v2);
	result -= vector2Arg(v1);
	if (result > PI) {
		result -= twoPI;
	}else if (result < -PI) {
		result += twoPI;
	}
	return result;
}

Vector2 vector2Negation(Vector2 p) {
	Vector2 result = { -p.x, -p.y };
	return result;
}

Vector2 vector2Normalize(Vector2 p) {
	double len = vector2Length(p);
	Vector2 result;
	result.x = (float) (p.x / len);
	result.y = (float) (p.y / len);
	return result;
}

Vector2 vector2Rotate(Vector2 p, double angle) {
	Vector2 result;
	result.x = (float) (p.x * cos(angle) - p.y * sin(angle));
	result.y = (float) (p.x * sin(angle) + p.y * cos(angle));
	return result;
}

Vector2 vector2Rotated(Vector2 p, double angle) {
	angle /= 180;
	angle *= PI;
	return vector2Rotate(p, angle);
}

Vector2 vector2Addition(Vector2 v1, Vector2 v2) {
	Vector2 result = { v1.x + v2.x, v1.y + v2.y };
	return result;
}

Vector2 vector2Subtraction(Vector2 v1, Vector2 v2) {
	Vector2 result = { v1.x - v2.x, v1.y - v2.y };
	return result;
}

Vector2 vector2Multiplication(Vector2 v1, float times) {
	Vector2 result = { v1.x * times, v1.y * times };
	return result;
}
