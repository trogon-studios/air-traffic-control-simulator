#include "stdafx.h"

char* stringInit( ) {
	char* newValue = (char*) malloc(sizeof(char) * 2);
	strcpy(newValue, "");
	return newValue;
}

char* intToString(int value) {
	int len = intMax((int) log10(abs(value)), 2) + 2;			//int len = _scprintf("%lf", value);
	char* newValue = (char*) malloc(sizeof(char)*(len + 1));
	sprintf(newValue, "%i", value);
	return newValue;
}

char* doubleToString(double value) {
	int len = intMax((int) log10(absd(value)), 2) + 8;			//int len = _scprintf("%lf", value);
	char* newValue = (char*) malloc(sizeof(char)*(len + 1));
	sprintf(newValue, "%lf", value);
	return newValue;
}

char* stringAppendChar(char* baseValue, char value, bool destroyBase) {
	char* newValue = NULL;
	if (baseValue != NULL) {
		int len = strlen(baseValue);
		newValue = (char*) malloc(sizeof(char)*(len + 2));
		sprintf(newValue, "%s%c", baseValue, value);
		if (destroyBase == true)
			free(baseValue);
	} else {
		newValue = (char*) malloc(sizeof(char) * 2);
		sprintf(newValue, "%c", value);
	}
	return newValue;
}

char* stringRemoveChar(char* baseValue, bool destroyBase) {
	if (baseValue != NULL) {
		if (strlen(baseValue) > 1) {
			char* newValue = stringSub(baseValue, 0, strlen(baseValue) - 1, destroyBase);
			return newValue;
		} else {
			if (destroyBase == true)
				free(baseValue);
			return NULL;
		}
	} else {
		return baseValue;
	}
}

char* stringSub(char* baseValue, size_t startIndex, size_t endIndex, bool destroyBase) {
	char* newValue = NULL;
	if (baseValue != NULL) {
		if (startIndex < 0) {
			fprintf(stderr, "stringSub: startIndex should be a positive value\n");
		} else if (endIndex <= startIndex) {
			fprintf(stderr, "stringSub: endIdnex should larger than startIndex\n");
		} else if (startIndex > (strlen(baseValue))) {
			fprintf(stderr, "stringSub: startIdnex should smaller than source length\n");
		} else if (endIndex > (strlen(baseValue) + 1)) {
			fprintf(stderr, "stringSub: endIdnex should smaller than or equal to source length\n");
		} else {
			int len = endIndex - startIndex;
			newValue = (char*) malloc(sizeof(char)*(len + 1));
			memset(newValue, '\0', len + 1);
			strncpy(newValue, baseValue + startIndex, len);
		}
		if (destroyBase == true)
			free(baseValue);
	}
	return newValue;
}

char* stringConcat(char* baseValue, char* value, bool destroyBase) {
	char* newValue = NULL;
	if (baseValue != NULL) {
		if (value != NULL) {
			int baseLen = strlen(baseValue);
			int len = strlen(value);
			newValue = (char*) malloc(sizeof(char)*(baseLen + len + 1));
			sprintf(newValue, "%s%s", baseValue, value);
			if (destroyBase == true)
				free(baseValue);
			return newValue;
		} else {
			return baseValue;
		}
	}
	return value;
}
