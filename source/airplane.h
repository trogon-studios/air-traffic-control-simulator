
#ifndef __airplane
#define __airplane

typedef struct _airplane {
	char* name;

	Vector2 currentPosition;
	Vector2 currentDirection;
	float currentAltitude;
	float currentSpeed;

	short status;

	short isApproaching;
	Vector2 approachDirection;

	Vector2 nextDirection;
	int nextAltitude;
	int nextSpeed;
} Airplane;

Airplane* airplaneInit( );

void airplaneUpdate(GameTime gameTime, Airplane* airplane);

void airplaneDraw(Airplane airplane);

Vector2 airplaneDirectionTranslate(int directionDegree);

void airplaneSetAltitude(Airplane* airplane, int newAltitude);

void airplaneSetDirection(Airplane* airplane, int newDirection);

void airplaneSetSpeed(Airplane* airplane, int newSpeed);

#endif
