#include "stdafx.h"

Screen* screenInit( ) {
	Screen* screen = (struct _screen*)malloc(sizeof(struct _screen));
	screen->isFullscreen = false;
	screen->width = 0;
	screen->height = 0;
	screen->aspectRatio = 0.0f;
	return screen;
}

void screenApply(Screen* screen, int width, int height) {
	screen->width = width;
	screen->height = height;
	screen->aspectRatio = (float) width / (float) height;

	glViewport(0, 0, screen->width, screen->height);
	screenOrthoClipping(*screen);
}

void screenOrtho(Screen screen) {
	glLoadIdentity();
	glOrtho(0, screen.width, 0, screen.height, 0, 1);
}

void screenOrthoAspect(Screen screen) {
	glLoadIdentity();
	if (screen.width >= screen.height) {
		gluOrtho2D(0, screen.width * screen.aspectRatio, 0, screen.height);
	} else {
		gluOrtho2D(0, screen.width, 0, screen.height / 2.0f / screen.aspectRatio);
	}
}

void screenOrthoNormal(Screen screen) {
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, 0, 1);
}

void screenOrthoAspectNormal(Screen screen) {
	glLoadIdentity();
	if (screen.width >= screen.height) {
		gluOrtho2D(0, 1.0 * screen.aspectRatio, 0, 1.0);
	} else {
		gluOrtho2D(0, 1.0, 0, 1.0 / screen.aspectRatio);
	}
}

void screenOrthoClipping(Screen screen) {
	glLoadIdentity();
	glOrtho(-screen.width / 2.0f, screen.width / 2.0f, -screen.height / 2.0f, screen.height / 2.0f, 0, 1);
}

void screenOrthoAspectClipping(Screen screen) {
	glLoadIdentity();
	if (screen.width >= screen.height) {
		gluOrtho2D(-screen.width / 2.0f * screen.aspectRatio, screen.width / 2.0f * screen.aspectRatio, -screen.height / 2.0f, screen.height / 2.0f);
	} else {
		gluOrtho2D(-screen.width / 2.0f, screen.width / 2.0f, -screen.height / 2.0f / screen.aspectRatio, screen.height / 2.0f / screen.aspectRatio);
	}
}

void screenOrthoNormalClipping(Screen screen) {
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, 0, 1);
}

void screenOrthoAspectNormalClipping(Screen screen) {
	glLoadIdentity();
	if (screen.width >= screen.height) {
		gluOrtho2D(-1.0 * screen.aspectRatio, 1.0 * screen.aspectRatio, -1.0, 1.0);
	} else {
		gluOrtho2D(-1.0, 1.0, -1.0 / screen.aspectRatio, 1.0 / screen.aspectRatio);
	}
}
