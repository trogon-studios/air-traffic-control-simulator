#include "stdafx.h"

const int keyBackspace = 8;
const int keyEnter = 13;
const int keyEscape = 27;
const int keyF1 = 0x0001;
const int keyF2 = 0x0002;
const int keyF3 = 0x0003;
const int keyF4 = 0x0004;
const int keyF5 = 0x0005;
const int keyF6 = 0x0006;
const int keyF7 = 0x0007;
const int keyF8 = 0x0008;
const int keyF9 = 0x0009;
const int keyF10 = 0x000A;
const int keyF11 = 0x000B;
const int keyF12 = 0x000C;
const int keyLeft = 0x0064;
const int keyUp = 0x0065;
const int keyRight = 0x0066;
const int keyDown = 0x0067;
const int keyPageUp = 0x0068;
const int keyPageDown = 0x0069;
const int keyHome = 0x006A;
const int keyEnd = 0x006B;
const int keyInsert	= 0x006C;
