#!/bin/bash
if [ -e "atcs.exe" ]
then
	rm "atcs.exe"
fi
gcc -o "atcs.exe" source/*.c -Wall -lm -lGL -lGLU -lglut -lm > "info.log" 2> "error.log"
if [ ! -s "info.log" ]
then
	rm "info.log"
fi
if [ -e "atcs.exe" ]
then
	if [ -e "error.log" ]
	then
		rm "error.log"
	fi
	./"atcs.exe"
fi
