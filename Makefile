CXX := gcc
DIR := source/
OBJDIR := obj/
FLAGS :=  -Wall -lGL -lGLU -lglut -lm
TARGET := acts
HEADERS := $(DIR)stdafx.h $(DIR)constrains.h $(DIR)color.h $(DIR)string.h $(DIR)vector2.h $(DIR)gameTime.h $(DIR)framework.h $(DIR)screen.h $(DIR)airplane.h $(DIR)sky.h $(DIR)game.h $(DIR)menu.h
OBJS := $(OBJDIR)constrains.o $(OBJDIR)color.o $(OBJDIR)string.o $(OBJDIR)vector2.o $(OBJDIR)gameTime.o $(OBJDIR)framework.o $(OBJDIR)screen.o $(OBJDIR)airplane.o $(OBJDIR)sky.o $(OBJDIR)game.o $(OBJDIR)menu.o $(OBJDIR)main.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(OBJS) $(FLAGS) -o $(TARGET)

$(OBJDIR)%.o: $(DIR)%.c $(HEADERS) $(OBJDIR)
	$(CXX) -c -o $@ $< $(FLAGS)

$(OBJDIR):
	mkdir $(OBJDIR)

clean:
	rm -rf $(OBJDIR)  $(TARGET)

exec:
	./$(TARGET)
